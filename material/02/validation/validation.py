from flask import Flask, _app_ctx_stack, render_template, flash, \
    redirect, url_for, request

from flask.ext.wtf import Form, TextField, PasswordField, \
    validators, SubmitField

from sqlalchemy import Table, MetaData, Column, Integer, String, create_engine
from sqlalchemy.orm import mapper, sessionmaker

import hashlib
import appconfig


# Flask Setting
app = Flask(__name__)
app.config.from_object(appconfig)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


# DB Setting
metadata = MetaData()
user = Table('user', metadata,
             Column('id', Integer, primary_key=True),
             Column('name', String(50)),
             Column('fullname', String(50)),
             Column('password', String(64))
             )


class User(object):
    def __init__(self, name, fullname, password):
        self.name = name
        self.fullname = fullname
        self.password = password

mapper(User, user)


# Form
class AddUserForm(Form):
    name = TextField('Name', [
        validators.Required(),
        validators.Length(min=2, max=50)]
    )
    fullname = TextField('Fullname', [
        validators.Required(),
        validators.Length(min=2, max=50)]
    )
    password = PasswordField('New Password', [
        validators.Required(),
        validators.Length(min=6),
        validators.EqualTo('confirm', message='Passwords must match')]
    )
    confirm = PasswordField('Repeat Password')
    submit = SubmitField('Send')


def init_db():
    """Creates the database tables."""
    with app.app_context():
        Session = get_db_session_maker()
        session = Session()
        engine = session.get_bind()

        metadata.drop_all(engine)
        metadata.create_all(engine)

        user = User(name='hubert',
                    fullname='Yi-Huan Chan',
                    password='secret')

        session.add(user)
        session.commit()


def get_db_session_maker():
    top = _app_ctx_stack.top
    if not hasattr(top, 'db_session_maker'):
        engine = create_engine(app.config['DBURL'])
        top.db_session_maker = sessionmaker(bind=engine)

    return top.db_session_maker


@app.teardown_appcontext
def close_db_connection(exception):
    """Closes the database again at the end of the request."""
    top = _app_ctx_stack.top
    if hasattr(top, 'db_session_maker'):
        top.db_session_maker.close_all()


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/users")
def users():
    Session = get_db_session_maker()
    session = Session()

    users = session.query(User).all()
    return render_template('users.html', users=users)


@app.route("/adduser", methods=['GET', 'POST'])
def adduser():
    form = AddUserForm()
    if request.method == 'POST' and form.validate():
        Session = get_db_session_maker()
        session = Session()

        m = hashlib.sha256()
        m.update(form.password.data)
        password = m.hexdigest()

        user = User(name=form.name.data,
                    fullname=form.fullname.data,
                    password=password)

        session.add(user)
        session.commit()

        flash('Thanks for adding user')
        return redirect(url_for('users'))

    return render_template('adduser.html', form=form)


if __name__ == "__main__":
    init_db()

    # Be careful !!!
    app.run(host='0.0.0.0', port=5001)
