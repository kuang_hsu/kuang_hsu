[An Introduction to Python's Flask Framework]: http://net.tutsplus.com/tutorials/python-tutorials/an-introduction-to-pythons-flask-framework/
[CSRF protection: do we have to generate a token for every form?]: http://stackoverflow.com/questions/8655817/csrf-protection-do-we-have-to-generate-a-token-for-every-form

# CSRF Examples

## Installation
- Read [An Introduction to Python's Flask Framework][An Introduction to Python's Flask Framework] first
- Use virtualenv
- pip install -r requirements.txt
- copy appconfig.py.default to appconfig.py
- Modify it for your environment
- python csrf.py
- Go to [http://localhost:5000](http://localhost:5000)


## CSRF POST Demo
1. Logout (clean session): [http://localhost:5000/logout](http://localhost:5000/logout)
2. Login (hubert/123456): [http://localhost:5000/login](http://localhost:5000/login)
3. Take a look for current status: [http://localhost:5000/bank](http://localhost:5000/bank)
4. Open attack/post.html in your browser
5. Bang!


## CSRF GET Demo
1. Logout (clean session): [http://localhost:5000/logout](http://localhost:5000/logout)
2. Login (hubert/123456): [http://localhost:5000/login](http://localhost:5000/login)
3. Take a look for current status: [http://localhost:5000/bank](http://localhost:5000/bank)
4. Open link [http://localhost:5000/get_transfer?id=2&amount=10000](http://localhost:5000/get_transfer?id=2&amount=10000)
5. Bang!


## Trick
- Use Url Shortener
- [GET CSRF Attack](http://goo.gl/Kxtr0)
- [POST CSRF Attack](http://goo.gl/VwFmp)


## Reference
- [An Introduction to Python's Flask Framework][An Introduction to Python's Flask Framework]
- [CSRF protection: do we have to generate a token for every form?][CSRF protection: do we have to generate a token for every form?]
