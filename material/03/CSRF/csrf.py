from flask import Flask, _app_ctx_stack, render_template, flash, \
    redirect, url_for, request, session

from sqlalchemy.orm.exc import NoResultFound

import appconfig

from db import init_db, get_db_session_maker
from db import User, Account

from utils import check_password_hashed

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

# Flask Setting
app = Flask(__name__)
app.config.from_object(appconfig)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


@app.teardown_appcontext
def close_db_connection(exception):
    """Closes the database again at the end of the request."""
    top = _app_ctx_stack.top
    if hasattr(top, 'db_session_maker'):
        top.db_session_maker.close_all()


@app.route('/')
def hello():
    return "Hello World!"


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        DBSession = get_db_session_maker(app)
        db_session = DBSession()

        try:
            user = db_session.query(User).filter(
                User.name == request.form['name'],
            ).one()
        except NoResultFound:
            flash('Login failed')
            return redirect(url_for('login'))

        if not check_password_hashed(request.form['password'], user.password):
            flash('Login failed')
            return redirect(url_for('login'))

        session['id'] = user.id
        session['name'] = user.name

        return redirect(url_for('bank'))

    if 'id' not in session:
        return render_template('login.html')
    else:
        return redirect(url_for('bank'))


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('login'))


@app.route('/bank')
def bank():
    DBSession = get_db_session_maker(app)
    db_session = DBSession()
    accounts = db_session.query(Account).all()
    return render_template('bank.html', accounts=accounts)


@app.route('/transfer', methods=['GET', 'POST'])
def transfer():
    if 'id' not in session:
        flash('Login First')
        return redirect(url_for('login'))

    if request.method == 'POST':
        from_id = session['id']
        to_id = int(request.form['id'])
        amount = int(request.form['amount'])

        return _transfer(from_id, to_id, amount)
    return render_template('transfer.html')


@app.route('/get_transfer')
def get_transfer():
    if 'id' not in session:
        flash('Login First')
        return redirect(url_for('login'))

    from_id = session['id']
    to_id = int(request.args['id'])
    amount = int(request.args['amount'])
    return _transfer(from_id, to_id, amount)


def _transfer(from_id, to_id, amount):
    DBSession = get_db_session_maker(app)
    db_session = DBSession()

    try:
        from_user = db_session.query(Account).filter(
            Account.user_id == from_id
        ).one()
        to_user = db_session.query(Account).filter(
            Account.user_id == to_id
        ).one()

        if amount > from_user.balance:
            flash('Money is not enough')
            return redirect(url_for('transfer'))

        from_user.balance = from_user.balance - amount
        to_user.balance = to_user.balance + amount

        db_session.commit()
        return redirect(url_for('bank'))

    except NoResultFound:
        flash('Cannot find user for id %s' % to_id)
        return redirect(url_for('transfer'))


if __name__ == "__main__":
    init_db(app)

    # Be careful !!!
    app.run(host='0.0.0.0')
