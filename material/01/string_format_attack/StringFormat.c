#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

typedef void (*ErrFunc)(unsigned long);

void GhastlyError(unsigned long err) 
{
  printf ("Unrecoverable error! - err = %lo\n", err);
  exit(-1);
}

void RecoverableError(unsigned long err)
{
  printf("Something went wrong, but you can fix it - err = %lo\n", err);
}

void PrintMessage(char* file, unsigned long err) 
{
  char buf[512] = {0};
  ErrFunc fErrFunc = GhastlyError;

  if (err == 5)
  {
    fErrFunc = GhastlyError;
  }
  else
  {
    fErrFunc = RecoverableError;
  }

  snprintf(buf, sizeof(buf) - 1, "Cannot find %s", file);
  printf("Address of fErrFunc is %p\n", &fErrFunc);

  fprintf(stdout, buf);

  printf("\nCalling ErrFunc %p\n", fErrFunc);
  fErrFunc(err);
}

void foo(void)
{
  printf("Augh! We've been hacked!\n");
}

int main(int argc, char *argv[])
{
  FILE *pFile;

  printf("Address of foo is %p\n", foo);

  pFile = fopen(argv[1], "r");

  if (pFile == NULL)
  {
    PrintMessage(argv[1], errno);
  }
  else
  {
    printf("Opened %s\n", argv[1]);
    fclose(pFile);
  }

  return 0;
}

