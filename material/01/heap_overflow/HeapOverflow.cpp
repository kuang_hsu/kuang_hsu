#include <cstdio>
#include <cstdlib>
#include <cstring>

class BadStringBuf
{
  public:
    BadStringBuf(void)
    {
      m_buf = NULL;
    }

    ~BadStringBuf(void)
    {
      if (m_buf != NULL)
        free(m_buf);
    }

    void Init(char *buf)
    {
      m_buf = buf;
    }

    void SetString(const char *input)
    {
      strcpy(m_buf, input);
    }

    const char* GetString(void)
    {
      return m_buf;
    }

  public:
    char* m_buf;
};

BadStringBuf* g_pInput = NULL;

void bar(void)
{
  printf("Augh! I've been hacked!\n");
}

int BadFunc(char* input1, char* input2)
{
  char* buf1 = NULL;
  char* buf2;
  int offset = 0x18;
  unsigned int retaddr = 0;

  buf2 = (char*)malloc(16);
  g_pInput = new BadStringBuf;
  buf1 = (char*)malloc(16);

  printf("buf1 = %p, buf2 = %p, g_pInput->m_buf = %p\n", buf1, buf2, &(g_pInput->m_buf));

  __asm__ ("movl %%ebp,%%eax" : "=a" (retaddr) );
  printf("ebp address = 0x%x, ret = 0x%x\n", retaddr, retaddr + 4);

  retaddr += 4;

  memset(input1, 0xfd, offset);

  // Little-Endian
  for (size_t i = 0; i < sizeof(u_long); i++)
    input1[offset+i] = ((u_long)retaddr >> (i * 8) & 255);
  input1[offset+sizeof(u_long)] = 0;

  g_pInput->Init(buf1);

  strcpy(buf2, input1);

  g_pInput->SetString(input2);
  return 0;
}

int main(int argc, char *argv[])
{
  char arg1[128];
  char arg2[5] = {0};

  void (*addr)() = bar;

  // Little-Endian
  for (size_t i = 0; i < sizeof(u_long); i++)
    arg2[i] = ((u_long)addr >> (i * 8) & 255);

  printf("Address of bar is %p\n", bar);
  BadFunc(arg1, arg2);

  delete g_pInput;

  return 0;
}

