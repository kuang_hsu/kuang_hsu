# Threat Modeling Homework


## XSS Threat Modeling Analysys
- 請以 STRIDE 分析我們在 Material 03 的 XSS 系統
    - 假定 id 為 1 的 hubert 是管理者
- 請以 attack 中的範例思考，可能會造成多嚴重的影響


## Amazon S3 REST Requests 協定分析
### Description
- [Signing and Authenticating REST Requests](http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html)
- 其中他的認證方式是透過 HTTP Header 中的 Authorization: AWS AWSAccessKeyId:Signature
- Signature 的計算方式包含許多不同的部份

### Requirement
- 請以 STRIDE 對 Signature 分析，如果排除以下進行計算會如何
    - HTTP-Verb
    - Content-MD5

### Example
- CanonicalizedResource
    - Spoofing: 除非 YourSecretAccessKeyID 外洩，所以基本上是安全
    - Tampering: 因為 Resource 沒有指定，當我偷聽到一個 DELETE request 之後，我可以把 resource 換成任意的資訊，就可以把這個帳號底下的文件砍光
    - Repudiation : 略
    - Information Disclosure: 只有偷聽到一個 GET request，就有機會可以讀到別的文件
    - Denial of Service: 把文件全砍了，應該也算某種 DoS
    - Elevation of Privilege: 無：不能提昇權限

### Extra Point
- 如果重新送出一個一樣的 S3 Request 會不會有問題？
- 為什麼設計上要加上 Expires 呢？
- 用 HTTPS 可以避免哪些問題？
