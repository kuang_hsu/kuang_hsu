#!/usr/bin/env python
# -*- coding: utf-8 -*-


def gen_salt():
    """ FIXME """
    salt = ''
    return salt


def gen_salted_hash(salt, origin_hash):
    """ FIXME """
    new_hash = ''
    return new_hash


def validate_pass(plain, passhash):
    """ FIXME """
    return True


def regenerate():
    def _make_pass(line):
        line = line.strip()
        (name, origin_hash) = line.split(':')

        salt = gen_salt()
        return "%s:%s\n" % (name, gen_salted_hash(salt, origin_hash))

    with open('password') as fr, open('newpass', 'w') as fw:
        for line in fr:
            fw.write(_make_pass(line))


def validate():
    plain_pass = {'hubert': '123456', 'tracy': '111111'}
    with open('newpass') as fr:
        for line in fr:
            line = line.strip()
            (name, passhash) = line.split(':')

            # Validate
            plain = plain_pass[name]
            assert(validate_pass(plain, passhash))


def main():
    regenerate()
    validate()

if __name__ == '__main__':
    main()
