# SQL Injection

## Description
- 在 material 中的 SQL Injection 專案中有明顯的 SQL Injection 漏洞

## Requirement
- 請修改 injection 以及 blind_injection 這兩個 function 避免 SQL Injection
- http://x.x.x.x:5002 是一台肉機，原始碼請見 material。他包含一個 blind injection 的漏洞，請使用 blind sql injection 技術找出 id 為 5 的資料為何
- 如果你是 QA，你該怎麼判斷 RD 的網址有沒有 SQL Injection，有什麼工具幫可以幫助你？
- 以工具測試你的修改是不是已經沒有漏洞
