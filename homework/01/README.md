# Buffer Overflow Homework

## Requirement
- 將 simple_http_parse.cpp 可能的 buffer overflow 修正
- 以 QA 的觀點撰寫產生 buffer overflow 的測試資料
- 產生 Makefile 或是 Visual Studio Solution File，如何打開警告，提示可能有 Buffer Overflow
- 找出適合的 static code analyzer 指出可能的問題

## Extra Point
- 將 material 中的程式在自己的環境實際執行過一次，請附上 gdb 以及貼圖
