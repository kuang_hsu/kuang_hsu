#include <iostream>
#include <string>
#include <cstring>
#include <cstdio>

struct http_request 
{
  /*
   * FIXME:
   * Base on RFC 2616: Section 3.2.1, The HTTP protocol
   * does not place any a priori limit on the length of a URI.
   */

  char    req_method[12];
  char    req_uri[256];
  char    req_protocol[5];
  int     req_major_version;
  int     req_minor_version;
};

int simple_http_request_line_parser(
    const char *request_line,
    struct http_request *parsed_request)
{
  char c = '\0';
  struct http_request tmp_request;

  memset (&tmp_request, 0, sizeof(tmp_request));

  if (request_line == NULL || parsed_request == NULL) {
    fprintf (stderr, "[%s] Cannot pass nullity check\n", __FUNCTION__);
    return -1;
  }

  /* Naive parsing */
  sscanf(request_line,
      "%s %s %4s/%d%c%d\r\n",
      tmp_request.req_method,
      tmp_request.req_uri,
      tmp_request.req_protocol,
      &(tmp_request.req_major_version),
      &c,
      &(tmp_request.req_minor_version));

  memcpy(parsed_request, &tmp_request, sizeof(*parsed_request));

  return 0;
}

int http_request_dump(struct http_request *parsed_request)
{
  char buf[256] = {0};

  if (parsed_request == NULL) {
    fprintf (stderr, "[%s] Cannot pass nullity check\n", __FUNCTION__);
    return -1;
  }

  sprintf(buf,
      "parsed method = %s\n"
      "parsed uri = %s\n"
      "parsed protocol = %s\n"
      "parsed major version = %d\n"
      "parsed minor version = %d\n",
      parsed_request->req_method,
      parsed_request->req_uri,
      parsed_request->req_protocol,
      parsed_request->req_major_version,
      parsed_request->req_minor_version
      );

  fprintf(stdout, buf);

  return 0;
}

int main(int argc, char *argv[])
{
  const char *request_line = "GET / HTTP/1.0\r\n";
  struct http_request req;

  simple_http_request_line_parser(request_line, &req);
  http_request_dump(&req);

  return 0;
}
