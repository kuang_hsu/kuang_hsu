[Cross-Site Request Forgery (CSRF) Prevention Cheat Sheet]: https://www.owasp.org/index.php/Cross-Site_Request_Forgery_%28CSRF%29_Prevention_Cheat_Sheet
[CSRF protection: do we have to generate a token for every form?]: http://stackoverflow.com/questions/8655817/csrf-protection-do-we-have-to-generate-a-token-for-every-form

# CSRF Homework


## Description
- Material 的程式碼有嚴重的 CSRF 漏洞，你可以修復他嗎？
- 對於 CSRF 漏洞，如果你是 QA，你該怎麼樣找到這些漏洞


## Requirements
- 修復 CSRF 的方法很多，你可以用 Form token 修復 GET 跟 POST 的 CSRF 漏洞
- 修復 CSRF 漏洞之後，使用者在 multi window/tab 會不會有使用上的不便
    - 例如同時打開 gmail 跟 google plus，我的 gmail 會不會無法送出 request（被視為 CSRF）


## Reference
- [Cross-Site Request Forgery (CSRF) Prevention Cheat Sheet][Cross-Site Request Forgery (CSRF) Prevention Cheat Sheet]
- [CSRF protection: do we have to generate a token for every form?][CSRF protection: do we have to generate a token for every form?]
